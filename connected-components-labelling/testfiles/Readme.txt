illusion.pbm
	- square spiral to check bounds, borders and object counter ( must be 1 )

empty.pbm
	- tricky! the padding has values! but it must be zero/empty

full_scale_dots.pbm
	- all possible number of objects in a 60 x 60 pixel PBM
	- tricky! the padding has values! must not be counted

asterisk.pbm
	- corrupted data, raster is smaller than the given dimension

square.pbm
	- recursive square output, checks in-between spaces

abstract.pbm
	- abstract! free draw objects at random, of any kind

hourglass.pbm
	- checks label depth, the hourglass should be counted as 1 object

cornercheck.pbm
	- left corner of the current row MUST NOT be connected to right corner of the previous row.
	- also checks dimension, raster data is bigger than dimension and it should read it.
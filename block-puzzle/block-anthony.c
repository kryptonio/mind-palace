/*********************************
* Name    : Anthony S. Deiparine *
* Section : SD22                 *
* Date    : 06/22/2019           *
*********************************/
#include <stdio.h>
#include <stdlib.h>

/* MAX_SIZE = (MAX_WIDTH * 30 + 1), '+1' means we don't use index 0 */
#define MAX_WIDTH 30
#define MAX_SIZE 901

/*
	Stack is a data structure that behaves like a pile of data.
	Stack and stack-memory behaves the same.
*/
typedef struct stack {
	short *data;
	short top; /* the top element of the stack, which is also the number of elements  */
} STACK;

/* 
	Heap is a data structure that acts like a priority queue. 
	Priority means that it can rearrange smallest first (min-heap) or largest first (max-heap) manner.
	It behaves like a balanced tree. Heap and heap-memory behaves different
*/
typedef struct heap {
	short *data;
	short cnt; /* number of elements of the heap */
} HEAP;

typedef struct puzzle {
	short width;
	short height;
	short blksize; /* the total size of the puzzle, starting at index 1 */
	short minblk; /* minimum number of blocks to be summed up */
	short *blk; /* the blocks in the puzzle */
	char *mark; /* markers for visited blocks */
	char *towers; /* the columns in the puzzle */
} PUZZLE;

void free_memory_pool(void *mem1, void *mem2, void *mem3, void *mem4, void *mem5)
{
	if(mem1) free(mem1);
	if(mem2) free(mem2);
	if(mem3) free(mem3);
	if(mem4) free(mem4);
	if(mem5) free(mem5);
}

char error(char* str, FILE *fp, PUZZLE *p, HEAP *hp, STACK *stk)
{
	printf("%s", str);
	free_memory_pool(p->blk, p->towers, p->mark, hp->data, stk->data);
	if(fp) fclose(fp);
	return 1;
}

void heap_insert(HEAP *hp, short cur)
{
	short temp = 0;
	short heap_cur = (hp->cnt)++;

	hp->data[heap_cur] = cur;

	/* compare and swap data from top to bottom */

	while(hp->data[heap_cur] < hp->data[heap_cur >> 1]) {
		temp = hp->data[heap_cur];
		hp->data[heap_cur] = hp->data[heap_cur >> 1];
		hp->data[heap_cur >> 1] = temp;
		heap_cur >>= 1;
	}
}

void heapify(HEAP *hp, short pos) 
{
	short left = pos << 1;
	short right = (pos << 1) + 1;
	short temp = 0;

	/* Rearrange the heap by putting the last element on the top, going to bottom */

	if(!((pos << 1) >= hp->cnt)) {
		if((hp->data[pos] > hp->data[left]) && (hp->data[left] < hp->data[right])) {
			temp = hp->data[pos];
			hp->data[pos] = hp->data[left];
			hp->data[left] = temp;
			heapify(hp, left);
		} else if((hp->data[pos] > hp->data[right]) && (hp->data[right] <= hp->data[left])) {
			temp = hp->data[pos];
			hp->data[pos] = hp->data[right];
			hp->data[right] = temp;
			heapify(hp, right);
		}
	}
}

short heap_get_next(HEAP *hp)
{
	short smallest = hp->data[1]; /* the first element is the smallest */

	hp->data[1] = hp->data[((hp->cnt)--) - 1];
	heapify(hp, 1);

	return smallest;
}

unsigned int connected_sums(PUZZLE *p, HEAP *hp, STACK *stk, short cur, short prev, short row)
{
	int left = cur - 1;
	int right = cur + 1;
	int top = cur - p->width;
	int bot = cur + p->width;

	if((cur > p->blksize - 1) || cur < 1) return 0; /* stop when current block is out of bounds */

	if(p->mark[cur]) return 0; /* if block is already visited */

	if(p->blk[prev] != p->blk[cur]) {
		if(prev == left)
			heap_insert(hp, cur);

		return 0;
	}

	if(cur > (row * p->width)) return 0;

	p->mark[cur] = 1; /* mark visited blocks */
	stk->data[(stk->top)++] = cur; /* add indexes to stack to zero-out later */

	/*
				[top]

		[left]  [cur] [right]

				[bot]
	*/

	/* check all except left edges */
	if(left == ((row - 1) * p->width) && cur != 1) 
		return p->blk[cur] + connected_sums(p, hp, stk, right, cur, row) /* right */
			+ connected_sums(p, hp, stk, bot, cur, row + 1) /* bottom */
			+ connected_sums(p, hp, stk, top, cur, row - 1); /* top */

	/* check all except right edges */
	if((cur + 1) == ((row * p->width) + 1)) {
		heap_insert(hp, cur + 1);
		return p->blk[cur] + connected_sums(p, hp, stk, bot, cur, row + 1) /* bottom */
			+ connected_sums(p, hp, stk, top, cur, row - 1) /* top */
			+ connected_sums(p, hp, stk, left, cur, row); /* left */
	}

	/* check all directions */
	return p->blk[cur] + connected_sums(p, hp, stk, right, cur, row) /* right */
		+ connected_sums(p, hp, stk, bot, cur, row + 1) /* bottom */
		+ connected_sums(p, hp, stk, top, cur, row - 1) /* top */
		+ connected_sums(p, hp, stk, left, cur, row); /* left */

}

short pull_down(PUZZLE *p, short height)
{
	short i = p->blksize - 1; /* lets begin from the last row going up */
	short limit = i - p->width; /* the indexes of the blocks in the last row */
	short start_row = height;
	short *zero_blk = 0; /* this will find and point to zero-valued blocks */
	short row_height = 0; /* this measures every tower in the puzzle */
	short upper = 0;
	char h = 0;

/*
		2 2 8 [7] 4 [5] 5 - start_row 	=====>			. . . [X] . [X] . 
		6 8 7 [7] 7 [5] 3				=====>			2 . . [X] . [X] 3 - start_row
		8 8 9 [9] 9 [5] 3				=====>			6 2 8 [X] 4 [X] 3
		|														|
		tower (7 towers)								towers (5 towers, 2 towers destroyed)
*/

	for(; i > limit; i--) { /* the indexes in the last row */

		if(p->towers[i - limit - 1]) continue; /* if a tower has destroyed (zero blocks), skip */

		upper = i - p->width;
		zero_blk = &p->blk[i];
		row_height = 0;

		if(p->blk[i] == 0) row_height++; /* every time a zero block is encountered, height increases */

		for(h = 1; h < height; h++) { /* current tower height */

			/* every time a zero block is encountered, height increases */
			if(p->blk[upper] == 0) row_height++;

			if(*zero_blk != 0) {
				zero_blk -= p->width; /* move upwards if block is non-zero */
			} else if(p->blk[upper] != 0) {
				*zero_blk = p->blk[upper];
				p->blk[upper] = 0;
				zero_blk -= p->width; /* move upwards after block swapping */
			}

			upper -= p->width; /* move upwards for the next block */
		}

		/* mark the columns without towers */
		if(row_height == height) p->towers[i - limit - 1] = 1;

		if(row_height < start_row) /* replace new smallest row */
			start_row = row_height;
	}

	return start_row;
}

void display_contents(short *data, short width, short start, short end)
{
	short idx = start;
	for(; idx < end; idx++) {
		(data[idx] == 0) ? printf(". ") : printf("%hu ", data[idx]);
		if(!(idx % width)) printf("\n");
	}
}

int main (int argc, char *argv[])
{
	FILE *fp = NULL;
	PUZZLE p = { 0 };
	STACK stk = { 0 };
	HEAP hp = { 0 };
	short row = 0;
	short idx = 0;
	short start_blk = 0;
	short used_blksize = 0;
	short used_width = 0;
	short block_sum = 0;
	short partial_sum = 0;
	short puzzle_sum = 0;

	if(argc != 2) 
		return error("USAGE: block-anthony (input filename)\n", fp, &p, &hp, &stk);

	if(!(fp = fopen(argv[1], "rb")))
		return error("IO ERROR: File does not exist or may be in use.\n", fp, &p, &hp, &stk);

	fscanf(fp, "%hu %hu %hu", &p.width, &p.height, &p.minblk);

	while(p.minblk || p.width || p.height) {

		if(p.width < 1 || p.height < 1 || p.width > 30 || p.height > 30 || p.minblk < 1 || p.minblk > 900)
			return error("PARSER ERROR: Parameter out of bounds.\n", fp, &p, &hp, &stk);

		/* do not use the blk[0] for easier math */
		p.blksize = (p.width * p.height) + 1;

		/* reuse memory if new allocation is smaller than currently used allocation */
		/* reset back if puzzle dimension is (30 x 30) */
		/* this is not proper solution because it needs statistical factor */
		if(p.blksize > used_blksize || (p.blksize != MAX_SIZE && used_blksize == MAX_SIZE)) {

			/* adjust allocation to (n + (n / 2)) to cater ordered allocation size */
			used_blksize = p.blksize + (p.blksize >> 1);

			/* do not re-use the allocation, free them */
			free_memory_pool(p.blk, hp.data, stk.data, p.mark, NULL);

			if(!(p.blk = malloc(used_blksize * sizeof(short))))
				return error("MEM ERROR: Unable to allocate enough memory.\n", fp, &p, &hp, &stk);

			if(!(hp.data = calloc(used_blksize, sizeof(short))))
				return error("MEM ERROR: Unable to allocate enough memory.\n", fp, &p, &hp, &stk);

			if(!(stk.data = calloc(used_blksize, sizeof(short))))
				return error("MEM ERROR: Unable to allocate enough memory.\n", fp, &p, &hp, &stk);

			if(!(p.mark = calloc(used_blksize, sizeof(char))))
				return error("MEM ERROR: Unable to allocate enough memory.\n", fp, &p, &hp, &stk);
		}

		/* reuse memory if new allocation is smaller than currently used allocation */
		if(p.width > used_width || used_width == MAX_WIDTH) {

			/* adjust allocation to (n + (n / 2)) to cater ordered allocation size */
			used_width = p.width + (p.width >> 1); 

			free(p.towers); /* do not re-use the allocation, free them */

			/* initialize puzzle tower labels */
			if(!(p.towers = malloc(used_width * sizeof(char))))
				return error("MEM ERROR: Unable to allocate enough memory.\n", fp, &p, &hp, &stk);
		}

		/* clear necessary variables */
		start_blk = stk.top = puzzle_sum = 0;
		hp.cnt = 1;

		for(idx = 0; idx < p.width; idx++) 
			p.towers[idx] = 0; /* clear towers */

		for(idx = 1; idx < p.blksize; idx++) fscanf(fp, "%hu", &p.blk[idx]); /* read file block by block */

		printf("Block size: %d x %d, Minimum blocks: %d\nOriginal blocks\n", p.width, p.height, p.minblk);
		display_contents(p.blk, p.width, 1, p.blksize);

		while(1) {

			/* clear all markings */
			for(idx = (start_blk * p.width + 1); idx < p.blksize; idx++) 
				p.mark[idx] = 0;

			/* traverse the puzzle */
			for(idx = (start_blk * p.width + 1); idx < p.blksize;) {

				row = (idx % p.width) ? (idx / p.width) + 1 : (idx / p.width);

				block_sum = connected_sums(&p, &hp, &stk, idx, idx, row); /* sum up connected blocks */

				if(stk.top >= p.minblk) {
					partial_sum += block_sum; /* sum up all connected block sums per puzzle phase */

					/* zero-out connected blocks, empty the stack */
					while(stk.top >= 0) p.blk[stk.data[--stk.top]] = 0;
				}

				stk.top = 0; /* clear stack */
				idx = heap_get_next(&hp); /* get smallest index in heap */
			}

			if(partial_sum == 0) break; /* no more connected blocks left, exit loop */

			/* gravity pull */
			start_blk += pull_down(&p, p.height - start_blk);

			puzzle_sum += partial_sum; /* add up all puzzle phase sum for final sum */
			partial_sum = 0;
		}

		printf("Final blocks\n");
		display_contents(p.blk, p.width, 1, p.blksize);

		printf("Total point: %d\n\n", puzzle_sum);

		fscanf(fp, "%hu %hu %hu", &p.width, &p.height, &p.minblk);

	} // end of while

	free_memory_pool(p.blk, hp.data, stk.data, p.mark, p.towers);
	fclose(fp);

	return 0;
}
